<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\GenericEmailService;
use Carbon\Carbon;

class MailController extends Controller
{
    
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        ini_set('max_execution_time', '0');
    }
    
    /**
     * Send email for reasigned appointments
     * 
     * @param \App\Http\Controllers\Request $request
     */
    public function reasignedAppointments(Request $request)
    {
        $days = explode(',', $request->input('days'));
        
        $appointments = ContractAppointment::with('register')
            ->where('status', 0)
            ->whereIn('appointment_date', $days)
            ->whereHas('register', function ($query) {
                $query->where('status', 11);
            })
            ->get();
            
        foreach ($appointments as $appointment) {
            $this->sendReasignedAppointmentEmail($appointment);
        }
        
        return response()->json(['emails_sent' => $appointments->count()]);
    }
    
    /**
     * Send reasigned appointment email
     * 
     * @param \App\Models\ContractAppointment $appointment
     */
    private function sendReasignedAppointmentEmail($appointment)
    {
        $emailTo = $appointment->register->load('user')->user->email;
        
        $viewVars = [
            'day' => Carbon::parse($appointment->appointment_date)->format('d/m/Y'),
            'hour' => Carbon::parse($appointment->appointment_hour)->format('h:i a')
        ];
        
        (new GenericEmailService)->send(
            $emailTo,
            'Cambio de fecha para firma de contrato',
            'reassigned_contract_appointment',
            $viewVars
        );
    }
}
