<?php

namespace App\Http\Controllers;

use App\Services\DocentesServices;
use App\Services\DocentesServicesUdg;


class DocentesController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    //GET LIST DOCENTES
    public function list(){
        $data = DocentesServices::list();
        return response()->json($data);
    }
    public function listUdg(){
        $data = DocentesServicesUdg::list();
        $newRegisters= [];
        foreach($data as $docente){
            $exists = DocentesServices::existsRegister($docente->curp);
            
            if($exists == 0){
                DocentesServicesUdg::modifyExist($docente->id);
                array_push($newRegisters,$docente);
            }
        }
        return response()->json($newRegisters);
    }
}
