<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocentesUdg extends Model
{
    public $timestamps = false;
    protected $table = 'docentes-udg';
    protected $fillable = ['nuevo'];

}
