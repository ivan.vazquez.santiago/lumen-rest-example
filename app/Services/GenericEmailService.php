<?php

namespace App\Services;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class GenericEmailService {

    /**
     * PHPMailer Instance
     *
     * @var \PHPMailer\PHPMailer\PHPMailer
     */
    private $email;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct() {
        $this->email = new PHPMailer(true);
    }

    /**
     * Send email
     *
     * @param array|string $to Email to (array of recipients or single email address)
     * @param string $subject Email subject
     * @param string $view Email view/template
     * @param array $viewVars Email data / view variables
     * @param array $attachments Attachments
     * @return mixed True when success. Exception message otherwise
     */
    public function send($to, $subject, $view, $viewVars, $attachments = [])
    {
        $data = $viewVars;
        try {
            $this->email->isSMTP();
            $this->email->isHTML(true);

            $this->email->SMTPDebug  = SMTP::DEBUG_OFF;
            $this->email->Host       = env('MAIL_HOST');
            $this->email->SMTPAuth   = !empty(env('MAIL_USERNAME'));
            $this->email->Username   = env('MAIL_USERNAME');
            $this->email->Password   = env('MAIL_PASSWORD');
            $this->email->Port       = env('MAIL_PORT');
            $this->email->SMTPSecure = env('MAIL_ENCRYPTION');
            $this->email->CharSet    = 'UTF-8';
            $this->email->Subject    = $subject;
            $this->email->Body       = view('emails.' . $view, $data);

            $this->email->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));

            $this->addRecipients($to);
            $this->addAttachments($attachments);
            $this->addBccAddresses();

            return $this->email->send();
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Add recipients
     *
     * @param array|string $recipients
     * @param \PHPMailer\PHPMailer\PHPMailer $mail
     */
    private function addRecipients($recipients)
    {
        if (is_array($recipients)) {
            foreach ($recipients as $recipient) {
                $this->email->addAddress(trim($recipient));
            }
        } else {
            $this->email->addAddress($recipients);
        }
    }

    /**
     * Add attachments
     *
     * @param array $attachments
     */
    private function addAttachments($attachments)
    {
        if (empty($attachments)) {
            return;
        }

        foreach($attachments as $attachment){
            $this->email->addAttachment($attachment);
        }
    }

    /**
     * Set BCC recipients
     */
    private function addBccAddresses()
    {
        if (empty(env('MAIL_BCC'))) {
            return;
        }

        $bccEmail = explode(',', env('MAIL_BCC'));

        foreach ($bccEmail as $bcc) {
            $this->email->addBCC(trim($bcc));
        }
    }
}
