<?php

namespace App\Services;

use App\Models\DocentesUdg;

abstract class DocentesServicesUdg
{
    public static function list() {
        $query = DocentesUdg::select('curp','id')->where('nuevo','0')->get();
        return $query;
    }

    public  static function modifyExist($id){
        DocentesUdg::find($id)->update(['nuevo' => 1]);
        return true;
    }
}
