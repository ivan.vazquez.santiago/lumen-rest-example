<?php

namespace App\Services;

use App\Models\Docentes;

abstract class DocentesServices
{
    public static function list() {
        $query = Docentes::whereNull('nombres')
        ->whereNull('pa')
        ->limit(10)
        ->get();
        return $query;
    }

    public static function existsRegister($curp){
        $query = Docentes::where('curp', $curp)->get();
        return $query->count();
    }
}
